from django.shortcuts import render
from .models import *
from django.contrib import messages
from django.shortcuts import redirect

from pprint import pprint

# Create your views here.

def index(request):
    return render(request, "index.html")

# Usuarios

def usuarios(request):
    usuarios = Usuario.objects.all()
    context = {
        'usuarios' : usuarios
    }
    return render(request, 'usuarios.html', context)

def guardar_usuario(request):
    apellidos = request.POST['apellidos']
    nombres = request.POST['nombres']
    correo_electronico = request.POST['correo_electronico']
    usuario = Usuario(apellidos = apellidos, nombres = nombres, correo_electronico = correo_electronico)
    usuario.save()
    messages.info(request, "USUARIO GUARDADO")

    usuarios = Usuario.objects.all()
    context = {
        'usuarios' : usuarios
    }
    return render(request, 'usuarios.html', context)

def editar_usuario(request, myid):
    usuario = Usuario.objects.get(id = myid)
    usuarios = Usuario.objects.all()
    context = {
        'usuario' : usuario,
        'usuarios' : usuarios
    }
    return render(request, 'usuarios.html', context)

def actualizar_usuario(request, myid):
    usuario = Usuario.objects.get(id = myid)
    usuario.apellidos = request.POST['apellidos']
    usuario.nombres = request.POST['nombres']
    usuario.correo_electronico = request.POST['correo_electronico']
    usuario.save()
    messages.info(request, "USUARIO ACTUALIZADO")
    return redirect (usuarios)

def eliminar_usuario(request, myid):
    usuario = Usuario.objects.get(id = myid)
    usuario.delete()
    messages.info(request, "USUARIO ELIMINADO")
    return redirect (usuarios)

# Tareas
def tareas(request):
    tareas = Tarea.objects.all()
    usuarios = Usuario.objects.all()
    context = {
        'usuarios' : usuarios,
        'tareas' : tareas
    }
    return render(request, 'tareas.html', context)

def guardar_tarea(request):
    nombre = request.POST['nombre']
    descripcion = request.POST['descripcion']
    fechaLimite = request.POST['fechaLimite']
    terminada = False
    usuario = request.POST['usuario']
    tarea = Tarea(nombre = nombre, descripcion = descripcion, fechaLimite = fechaLimite, terminada = terminada, usuario = usuario)

    pprint(dir(tarea))

    tarea.save()
    messages.info(request, "TAREA GUARDADA")


    tareas = Tarea.objects.all()
    usuarios = Usuario.objects.all()
    context = {
        'usuarios' : usuarios,
        'tareas' : tareas
    }
    
    return render(request, 'tareas.html', context)
    

def editar_tarea(request, myid):
    tarea = Tarea.objects.get(id = myid)
    tareas = Tarea.objects.all()
    context = {
        'tarea' : tarea,
        'tareas' : tareas
    }
    return render(request, 'tareas.html', context)

def actualizar_tarea(request, myid):
    tarea = Tarea.objects.get(id = myid)
    tarea.nombre = request.POST['nombre']
    tarea.descripcion = request.POST['descripcion']
    tarea.fechaLimite = request.POST['fechaLimite']
    tarea.save()
    messages.info(request, "TAREA ACTUALIZADA")
    return redirect (tareas)

def terminar_tarea(request, myid):
    tarea = Tarea.objects.get(id = myid)
    tarea.terminada = True
    tarea.save()
    messages.info(request, "TAREA TERMINADA")
    return redirect (tareas)

def eliminar_tarea(request, myid):
    tarea = Tarea.objects.get(id = myid)
    tarea.delete()
    messages.info(request, "TAREA ELIMINADA")
    return redirect (tareas)






